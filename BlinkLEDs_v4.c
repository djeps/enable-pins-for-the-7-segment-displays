// The following #define is needed for the delay timing to work properly
#define F_CPU 16000000UL

// The following #define is needed so the _delay_ms accepts not only consts
#define __DELAY_BACKWARD_COMPATIBLE__

#include <avr/io.h>
#include <util/delay.h>

int main(void)
{
	uint16_t delay = 1;
	
	DDRD = (1 << PD0) | (1 << PD2) | (1 << PD3) | (1 << PD4) | (1 << PD5) | (1 << PD6) | (1 << PD7); // DDRD |= 0b11111101;
	DDRB = (1 << PB0) | (1 << PB1);                                                                  // DDRB |= 0b00000011;
	
	ADMUX = (1 << REFS0) | (1 << ADLAR);
	ADCSRA = (1 << ADEN) | (1 << ADPS1) | (1 << ADPS0);
	ADCSRB = 0x00;
	
	while (1)
	{
		ADCSRA |= (1 << ADSC);
		while (ADCSRA & (1 << ADSC)); // Wait until conversion is finished
		delay = ADCH; // We are using only an 8 bit resolution, so this should give us a range of 0 - 255 milliseconds
		
		PORTD |= 1 << PD0;
		_delay_ms(delay);
		PORTD &= 0 << PD0;
		_delay_ms(delay);
		
		PORTD |= 1 << PD2;
		_delay_ms(delay);
		PORTD &= 0 << PD2;
		_delay_ms(delay);
		
		PORTD |= 1 << PD3;
		_delay_ms(delay);
		PORTD &= 0 << PD3;
		_delay_ms(delay);
		
		PORTD |= 1 << PD4;
		_delay_ms(delay);
		PORTD &= 0 << PD4;
		_delay_ms(delay);
		
		PORTD |= 1 << PD5;
		_delay_ms(delay);
		PORTD &= 0 << PD5;
		_delay_ms(delay);
		
		PORTD |= 1 << PD6;
		_delay_ms(delay);
		PORTD &= 0 << PD6;
		_delay_ms(delay);
		
		PORTD |= 1 << PD7;
		_delay_ms(delay);
		PORTD &= 0 << PD7;
		_delay_ms(delay);
		
		PORTB |= 1 << PB0;
		_delay_ms(delay);
		PORTB &= 0 << PB0;
		_delay_ms(delay);
		
		PORTB |= 1 << PB1;
		_delay_ms(delay);
		PORTB &= 0 << PB1;
		_delay_ms(delay);
	}
}
