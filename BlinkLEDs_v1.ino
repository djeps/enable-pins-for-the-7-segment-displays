const int DELAY_PIN = A0;

int delayTime = 0;

void setup() {
  pinMode(0, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  
  pinMode(DELAY_PIN, INPUT);
}

void loop() {
  delayTime = analogRead(DELAY_PIN);

  digitalWrite(0, HIGH);
  delay(delayTime);
  digitalWrite(0, LOW);
  delay(delayTime);

  digitalWrite(2, HIGH);
  delay(delayTime);
  digitalWrite(2, LOW);
  delay(delayTime);

  digitalWrite(3, HIGH);
  delay(delayTime);
  digitalWrite(3, LOW);
  delay(delayTime);

  digitalWrite(4, HIGH);
  delay(delayTime);
  digitalWrite(4, LOW);
  delay(delayTime);

  digitalWrite(5, HIGH);
  delay(delayTime);
  digitalWrite(5, LOW);
  delay(delayTime);
  
  digitalWrite(6, HIGH);
  delay(delayTime);
  digitalWrite(6, LOW);
  delay(delayTime);
  
  digitalWrite(7, HIGH);
  delay(delayTime);
  digitalWrite(7, LOW);
  delay(delayTime);
  
  digitalWrite(8, HIGH);
  delay(delayTime);
  digitalWrite(8, LOW);
  delay(delayTime);
  
  digitalWrite(9, HIGH);
  delay(delayTime);
  digitalWrite(9, LOW);
  delay(delayTime);
}
