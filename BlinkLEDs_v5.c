// The following #define is needed for the delay timing to work properly
#define F_CPU 16000000UL

// The following #define is needed so the _delay_ms accepts not only consts
#define __DELAY_BACKWARD_COMPATIBLE__

#include <avr/io.h>
#include <util/delay.h>

int main(void)
{
	uint16_t delay = 1;
	
	DDRD = (1 << PD0) | (1 << PD2) | (1 << PD3) | (1 << PD4) | (1 << PD5) | (1 << PD6) | (1 << PD7); // DDRD |= 0b11111101;
	DDRB = (1 << PB0) | (1 << PB1);                                                                  // DDRB |= 0b00000011;
	
	ADMUX = (1 << REFS0) | (1 << ADLAR);
	ADCSRA = (1 << ADEN) | (1 << ADPS1) | (1 << ADPS0);
	ADCSRB = 0x00;
	
	while (1)
	{
		ADCSRA |= (1 << ADSC);
		while (ADCSRA & (1 << ADSC)); // Wait until the ADC conversion has finished
		delay = ADCH; // We are using only an 8 bit resolution, so this should give us a range of 0 - 255 milliseconds
		
		for (uint8_t pin = PD0; pin <= PD7; pin++)
		{
			// We skip all these pins
			if (pin == PD1)
			{
				continue;
			}
			
			PORTD |= 1 << pin;
			_delay_ms(delay);
			PORTD &= 0 << pin;
			_delay_ms(delay);
		}
		
		for (uint8_t pin = PB0; pin <= PB7; pin++)
		{
			// We skip all these pins
			if ((pin == PB2) || (pin == PB3) || (pin == PB4) || (pin == PB5) || (pin == PB6) || (pin == PB7)) 
			{
				continue;
			}
			
			PORTB |= 1 << pin;
			_delay_ms(delay);
			PORTB &= 0 << pin;
			_delay_ms(delay);
		}
	}
}
