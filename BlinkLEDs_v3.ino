const int EN_PINS[9] = {0, 2, 3, 4, 5, 6, 7, 8, 9};
const int DELAY_PIN = A0;

int delayTime = 0;

void setup() {
  int i = 0;
  while (i < 10) {
    pinMode(EN_PINS[i], OUTPUT);
    i++;
  }
  
  pinMode(DELAY_PIN, INPUT);
}

void loop() {
  delayTime = analogRead(DELAY_PIN);

  int i = 0;
  while (i < 10) {
    digitalWrite(EN_PINS[i], HIGH);
    delay(delayTime);
    digitalWrite(EN_PINS[i], LOW);
    delay(delayTime);
    i++;
  }
}
