const int EN_PINS[9] = {0, 2, 3, 4, 5, 6, 7, 8, 9};
const int DELAY_PIN = A0;

int delayTime = 0;

void setup() {
  for (int i = 0; i < 10; i++) {
    pinMode(EN_PINS[i], OUTPUT);
  }
  
  pinMode(DELAY_PIN, INPUT);
}

void loop() {
  delayTime = analogRead(DELAY_PIN);

  for (int i = 0; i < 10; i++) {
    digitalWrite(EN_PINS[i], HIGH);
    delay(delayTime);
    digitalWrite(EN_PINS[i], LOW);
    delay(delayTime);
  }
}
